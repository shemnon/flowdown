#FlowDown

Flowdown started as a tech demo for the new
TextFlow node in JavaFX.  It renders
[MarkDown](http://markdown.org) but instead
of using HTML it uses JavaFX. The name was
origianlly picked as a portmadou (sp) of
TextFlow and MarkDown, but it is actually a
real word in the US Legal system:

> A flow down clause is a contract provision
> by which  the parties incorporate the terms
> of the general contract between the owner
> and the general contractor into the lower
> tier agreement. It may also be referred to
> as a pass-through or conduit clause. They
> are most common in construction contracts.
> Such provisions state that the
> subcontractor is bound to the contractor
> in the same manner as the contractor is
> bound to the owner in the prime contract.
> Flow-down provisions help to ensure that
> the subcontractor's obligations to the
> contractor mirror the contractor's
> obligations to the owner.
> \[ [USLegal](http://definitions.uslegal.com/f/flow-down-clause/) \]


So rather than HTML being the subcontractor
JavaFX is the subcontractor.