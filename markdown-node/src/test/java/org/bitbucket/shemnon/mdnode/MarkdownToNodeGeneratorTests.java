/*
 * Copyright (c) 2012, Danno Ferrin
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Danno Ferrin nor the
 *         names of contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.bitbucket.shemnon.mdnode;

import org.junit.Test;

import java.io.*;

/**
 *
 * @author shemnon
 */
public class MarkdownToNodeGeneratorTests {
    
     @Test
     public void smoke() {
         MarkdownToNodeGenerator gen = new MarkdownToNodeGenerator();
         
         gen.createMarkdownNode("This is a simple test.");
     }

     @Test
     public void smokier() {
         MarkdownToNodeGenerator gen = new MarkdownToNodeGenerator();
         
         gen.createMarkdownNode(
                   "This is a not so simple test.\n"
                 + "=============================\n"
                 + "\n"
                 + "It *was* a \"simple test\" until I looked into some more of the work.\n"
                 + "**a lot** more random stuff.\n"
                 + "\n"
                 + "And by [stuff](http://example.com/) I mean [[Stuff]] and [Stuff][ref]\n"
                 + "\n"
                 + "here is some `code` inline\n"
                 + "\n"
                 + "    some other code\n"
                 + "    in a long block\n"
                 + "\n"
                 + "here is a different code inline\n"
                 + "```java\n"
                 + "some other code\n"
                 + "in a long block\n"
                 + "```\n"
                 + "\n"
                 + "---------\n"
                 + "\n"
                 + "minor header\n"
                 + "------------\n"
                 + "\n"
                 + "[ref]: http:/example.com/ref\n");
     }

    @Test
    public void smokeBasics() {
        InputStream is = MarkdownToNodeGeneratorTests.class.getResourceAsStream("/org/bitbucket/shemnon/mdnode/MarkDownBasics.md");
        String s = readCompletely(is);

        MarkdownToNodeGenerator gen = new MarkdownToNodeGenerator();

        gen.createMarkdownNode(s);
    }

    private String readCompletely(InputStream is) {
        Reader r = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(r);
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
