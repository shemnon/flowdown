/*
 * Copyright (c) 2012, Danno Ferrin
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Danno Ferrin nor the
 *         names of contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.bitbucket.shemnon.mdnode;

import javafx.scene.layout.VBox;
import org.pegdown.PegDownProcessor;
import org.pegdown.ast.RootNode;

/**
 *
 * @author shemnon
 */
public class MarkdownToNodeGenerator {

    public static final String STYLE_CLASS_BODY = "md-body";
    public static final String STYLE_CLASS_CODE = "md-code";
    public static final String STYLE_CLASS_EMPH = "md-emph";
    public static final String STYLE_CLASS_STRONG = "md-strong";
    public static final String STYLE_CLASS_HYPERLINK = "md-hyperlink";
    public static final String STYLE_CLASS_HEADER = "md-header";
    public static final String STYLE_CLASS_HEADER_BASE = "md-header-";
    public static final String STYLE_CLASS_VERBATIM = "md-verbatim";
    public static final String STYLE_CLASS_PARA = "md-para";
    public static final String STYLE_CLASS_BLOCKQUOTE = "md-blockquote";
    public static final String STYLE_CLASS_ORDERED_LIST = "md-ordered-list";
    public static final String STYLE_CLASS_UNORDERED_LIST = "md-unordered-list";
    public static final String STYLE_CLASS_BULLET = "md-bullet";
    public static final String STYLE_CLASS_LIST_CONTENT = "md-list-content";
    public static final String STYLE_CLASS_SEPARATOR = "md-separator";

    public static final String STYLE_CLASS_TABLE = "md-table";
    public static final String STYLE_CLASS_TABLE_HEADER = "md-table-header";
    public static final String STYLE_CLASS_TABLE_ROW = "md-table-row";
    public static final String STYLE_CLASS_TABLE_CAPTION = "md-table-caption";

    public static final int DEFAULT_PARSER_FLAGS = 0;

    protected int parserFlags;

    protected MarkdownVisitor visitor;

    public javafx.scene.Node createMarkdownNode(String markdown) {
        if (markdown == null) {
            return null;
        }

        PegDownProcessor processor = new PegDownProcessor(parserFlags);

        MarkdownVisitor ourVisitor = visitor;
        if (ourVisitor == null) {
            ourVisitor = new MarkdownVisitor();
            //ourVisitor = new DebugVisitor();
        }

        VBox result = new VBox();
        ourVisitor.pushNode(result);

        RootNode root = processor.parseMarkdown(markdown.toCharArray());

        root.accept(ourVisitor);

        result.getStyleClass().setAll(STYLE_CLASS_BODY);
        result.layout(); // this is needed to get the list box bullets to align properly

        return result;
    }

    public int getParserFlags() {
        return parserFlags;
    }

    public void setParserFlags(int parserFlags) {
        this.parserFlags = parserFlags;
    }

    public MarkdownVisitor getMarkdownVisitor() {
        return visitor;
    }

    public void setMarkdownVisitor(MarkdownVisitor visitor) {
        this.visitor = visitor;
    }
}
