package org.bitbucket.shemnon.mdnode;

import org.pegdown.ast.*;

/**
 * Created with IntelliJ IDEA.
 * User: shemnon
 * Date: 3 Mar 2013
 * Time: 11:13 AM
 */
public class DebugVisitor extends MarkdownVisitor {

    int indentLevel = 0;
    String indent = "                                                                  ";

    public void invoke (Runnable mh, String name) {
        System.out.print(indent.substring(0, indentLevel));
        System.out.println(name);

        indentLevel += 2;
        try {
            mh.run();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        indentLevel -= 2;

        System.out.print(indent.substring(0, indentLevel));
        System.out.print('/');
        System.out.println(name);

    }

    @Override
    public void visit(AbbreviationNode node) {
        invoke(() -> super.visit(node), "abbreviation");
    }

    @Override
    public void visit(AutoLinkNode node) {
        invoke(() -> super.visit(node), "autoLink");
    }

    @Override
    public void visit(BlockQuoteNode node) {
        invoke(() -> super.visit(node), "BlockQuote");
    }

    @Override
    public void visit(BulletListNode node) {
        invoke(() -> super.visit(node), "BulletList");
    }

    @Override
    public void visit(CodeNode node) {
        invoke(() -> super.visit(node), "Code");
    }

    @Override
    public void visit(DefinitionListNode node) {
        invoke(() -> super.visit(node), "DefinitionList");
    }

    @Override
    public void visit(DefinitionNode node) {
        invoke(() -> super.visit(node), "Definition");
    }

    @Override
    public void visit(DefinitionTermNode node) {
        invoke(() -> super.visit(node), "DefinitionTerm");
    }

    @Override
    public void visit(EmphNode node) {
        invoke(() -> super.visit(node), "Emph");
    }

    @Override
    public void visit(ExpImageNode node) {
        invoke(() -> super.visit(node), "ExpImage");
    }

    @Override
    public void visit(ExpLinkNode node) {
        invoke(() -> super.visit(node), "ExpLink");
    }

    @Override
    public void visit(HeaderNode node) {
        invoke(() -> super.visit(node), "Header");
    }

    @Override
    public void visit(HtmlBlockNode node) {
        invoke(() -> super.visit(node), "HtmlBlock");
    }

    @Override
    public void visit(InlineHtmlNode node) {
        invoke(() -> super.visit(node), "InlineHTML");
    }

    @Override
    public void visit(ListItemNode node) {
        invoke(() -> super.visit(node), "ListItem");
    }

    @Override
    public void visit(MailLinkNode node) {
        invoke(() -> super.visit(node), "MailLinkNode");
    }

    @Override
    public void visit(Node node) {
        invoke(() -> super.visit(node), "Node");
    }

    @Override
    public void visit(OrderedListNode node) {
        invoke(() -> super.visit(node), "OrderedList");
    }

    @Override
    public void visit(ParaNode node) {
        invoke(() -> super.visit(node), "Para");
    }

    @Override
    public void visit(QuotedNode node) {
        invoke(() -> super.visit(node), "Quoted");
    }

    @Override
    public void visit(ReferenceNode node) {
        invoke(() -> super.visit(node), "Reference");
    }

    @Override
    public void visit(RefImageNode node) {
        invoke(() -> super.visit(node), "RefImage");
    }

    @Override
    public void visit(RefLinkNode node) {
        invoke(() -> super.visit(node), "RefLink");
    }

    @Override
    public void visit(RootNode node) {
        invoke(() -> super.visit(node), "Root");
    }

    @Override
    public void visit(SimpleNode node) {
        invoke(() -> super.visit(node), "Simple");
    }

    @Override
    public void visit(SpecialTextNode node) {
        invoke(() -> super.visit(node), "SpecialText");
    }

    @Override
    public void visit(StrongNode node) {
        invoke(() -> super.visit(node), "Strong");
    }

    @Override
    public void visit(SuperNode node) {
        invoke(() -> super.visit(node), "Super");
    }

    @Override
    public void visit(TableBodyNode node) {
        invoke(() -> super.visit(node), "TableBody");
    }

    @Override
    public void visit(TableCellNode node) {
        invoke(() -> super.visit(node), "TableCell");
    }

    @Override
    public void visit(TableColumnNode node) {
        invoke(() -> super.visit(node), "TableColumn");
    }

    @Override
    public void visit(TableCaptionNode node) {
        invoke(() -> super.visit(node), "TableCaption");
    }

    @Override
    public void visit(TableHeaderNode node) {
        invoke(() -> super.visit(node), "TableHeader");
    }

    @Override
    public void visit(TableNode node) {
        invoke(() -> super.visit(node), "Table");
    }

    @Override
    public void visit(TableRowNode node) {
        invoke(() -> super.visit(node), "TableRow");
    }

    @Override
    public void visit(TextNode node) {
        invoke(() -> super.visit(node), "Text");
    }

    @Override
    public void visit(VerbatimNode node) {
        invoke(() -> super.visit(node), "Verbatim");
    }

    @Override
    public void visit(WikiLinkNode node) {
        invoke(() -> super.visit(node), "WikiLink");
    }

}
