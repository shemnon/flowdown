package org.bitbucket.shemnon.mdnode;

import org.bitbucket.shemnon.OffThreadTransformingPane;

/**
 * Created with IntelliJ IDEA.
 * User: shemnon
 * Date: 2 Feb 2013
 * Time: 1:57 PM
 */

public class MarkdownNode extends OffThreadTransformingPane<String> {

    public static final String STYLESHEET_FLOWDOWN = "FlowDown.css";
    public static final String STYLESHEET_GITHUB = "Github.css";
    public static final String STYLESHEET_DEFAULT = STYLESHEET_GITHUB;

    protected MarkdownToNodeGenerator generator;

    public MarkdownNode() {
        this(new MarkdownToNodeGenerator());
        getStylesheets().setAll(MarkdownToNodeGenerator.class.getResource(STYLESHEET_DEFAULT).toExternalForm());
    }

    public MarkdownNode(MarkdownToNodeGenerator generator) {
        this.generator = generator;
        setTransformation(this.generator::createMarkdownNode);
    }

    public void setParserFlags(int flags) {
        int oldFlags = generator.getParserFlags();
        if (oldFlags != flags) {
            generator.setParserFlags(flags);
            recalculateTransform();
        }
    }

    public int getParserFlags() {
        return generator.getParserFlags();
    }
}
