package org.bitbucket.shemnon.mdnode;

import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import org.pegdown.ast.*;

import java.util.*;

/**
* Created with IntelliJ IDEA.
* User: shemnon
* Date: 2 Feb 2013
* Time: 8:56 AM
*/
public class MarkdownVisitor implements Visitor {

    Set<String> cssClasses = new TreeSet<>();

    protected Deque<Pane> nodeStack = new LinkedList<>();
    Map<String, ReferenceNode> references = new HashMap<>();
    protected Pane currentCollector;

    LinkedList<Integer> listCount = new LinkedList<>();
    LinkedList<Integer> tableRowCount = new LinkedList<>();
    LinkedList<Integer> tableColumnCount = new LinkedList<>();

    public void pushNode(Pane n) {
        nodeStack.push(n);
        currentCollector = n;
    }

    public void popNode() {
        if (!nodeStack.isEmpty()) {
            nodeStack.pop();
        }
        currentCollector = nodeStack.peek();
    }

    void visitChildren(Node node) {
        if (node == null) return; // defensive parsing
        for (Node child : node.getChildren()) {
            child.accept(this);
        }
    }

    void addTextNode(String text) {
        // strip trailing newlines, they cause problems
        while (text.endsWith("\n")) {
            text = text.substring(0, text.length()-1);
        }
        Text t = new Text(text);
        t.getStyleClass().setAll(cssClasses);
        currentCollector.getChildren().add(t);
    }


    @Override
    public void visit(SuperNode node) {
        TextFlow tf = new TextFlow();
        currentCollector.getChildren().add(tf);
        pushNode(tf);
        visitChildren(node);
        popNode();
    }

    @Override
    public void visit(TextNode node) {
        addTextNode(node.getText());
    }


    @Override
    public void visit(HtmlBlockNode node) {
        // no HTML handling
        addTextNode(node.getText());
    }

    @Override
    public void visit(InlineHtmlNode node) {
        // no HTML handling
        addTextNode(node.getText());
    }

    @Override
    public void visit(StrongNode node) {
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_STRONG);
        visitChildren(node);
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_STRONG);
    }

    @Override
    public void visit(EmphNode node) {
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_EMPH);
        visitChildren(node);
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_EMPH);
    }

    @Override
    public void visit(CodeNode node) {
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_CODE);
        TextFlow tf = new TextFlow();
        tf.getStyleClass().setAll(cssClasses);
        currentCollector.getChildren().add(tf);
        pushNode(tf);
        addTextNode(node.getText());
        popNode();
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_CODE);
    }

    @Override
    public void visit(QuotedNode node) {
        switch (node.getType()) {
            case DoubleAngle:
                addTextNode("\u00AB");
                visitChildren(node);
                addTextNode("\u00BB");
                break;
            case Double:
                addTextNode("\u201C");
                visitChildren(node);
                addTextNode("\u201D");
                break;
            case Single:
                addTextNode("\u2018");
                visitChildren(node);
                addTextNode("\u2019");
                break;
        }

    }

    @Override
    public void visit(SimpleNode node) {
        switch (node.getType()) {
            case Apostrophe: addTextNode("\u2019"); break;
            case Ellipsis: addTextNode("\u2026"); break;
            case Emdash: addTextNode("\u2014"); break;
            case Endash: addTextNode("\u2013"); break;
            case Nbsp: addTextNode("\u00a0"); break;


            case Linebreak:
                popNode();
                TextFlow tf = new TextFlow();
                tf.getStyleClass().setAll(cssClasses);
                currentCollector.getChildren().add(tf);
                pushNode(tf);
                break;
            case HRule:
                Separator sep = new Separator();
                sep.getStyleClass().add(MarkdownToNodeGenerator.STYLE_CLASS_SEPARATOR);
                currentCollector.getChildren().add(sep);
                break;
        }
    }

    @Override
    public void visit(SpecialTextNode node) {
        addTextNode(node.getText());
    }


    @Override
    public void visit(ParaNode node) {
        VBox vbox = new VBox();
        vbox.getStyleClass().setAll(cssClasses);
        vbox.getStyleClass().add(MarkdownToNodeGenerator.STYLE_CLASS_PARA);
        currentCollector.getChildren().add(vbox);
        pushNode(vbox);
        visitChildren(node);
        popNode();
    }

    @Override
    public void visit(BlockQuoteNode node) {
        VBox vBox = new VBox();
        vBox.getStyleClass().setAll(cssClasses);
        vBox.getStyleClass().add(MarkdownToNodeGenerator.STYLE_CLASS_BLOCKQUOTE);
        currentCollector.getChildren().add(vBox);
        pushNode(vBox);
        visitChildren(node);
        popNode();
    }

    @Override
    public void visit(VerbatimNode node) {
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_VERBATIM);
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_VERBATIM + "-" + node.getType());
        TextFlow tf = new TextFlow();
        tf.getStyleClass().setAll(cssClasses);
        pushNode(tf);
        addTextNode(node.getText());
        popNode();
        currentCollector.getChildren().add(tf);
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_VERBATIM);
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_VERBATIM + "-" + node.getType());
    }

    void startListBox(String cssClass) {
        VBox vbox = new VBox();
        vbox.getStyleClass().setAll(cssClasses);
        vbox.getStyleClass().add(cssClass);

        currentCollector.getChildren().add(vbox);
        vbox.setMinHeight(Region.USE_PREF_SIZE);
        vbox.setMaxHeight(Region.USE_PREF_SIZE);

        pushNode(vbox);
    }

    void startListRow(String bullet) {
        Text bt = new Text(bullet);
        bt.setTextAlignment(TextAlignment.RIGHT);
        bt.setTextOrigin(VPos.BASELINE);
        bt.getStyleClass().setAll(cssClasses);
        bt.getStyleClass().add(MarkdownToNodeGenerator.STYLE_CLASS_BULLET);

        VBox bulletContent = new VBox();
        bulletContent.setMinHeight(Region.USE_PREF_SIZE);
        bulletContent.setMaxHeight(Region.USE_PREF_SIZE);
        bulletContent.getStyleClass().setAll(cssClasses);
        bulletContent.getStyleClass().add(MarkdownToNodeGenerator.STYLE_CLASS_LIST_CONTENT);

        HBox hb = new HBox();
        hb.setMinHeight(Region.USE_PREF_SIZE);
        hb.setMaxHeight(Region.USE_PREF_SIZE);
        hb.setAlignment(Pos.BASELINE_LEFT);

        hb.getChildren().setAll(bt, bulletContent);

        currentCollector.getChildren().add(hb);

        pushNode(bulletContent);
    }

    void stopListRow() {
        popNode();
    }

    void stopListBox() {
        popNode();
    }

    @Override
    public void visit(BulletListNode node) {
        startListBox(MarkdownToNodeGenerator.STYLE_CLASS_UNORDERED_LIST);
        listCount.push(null);
        visitChildren(node);
        listCount.pop();
        stopListBox();
    }

    @Override
    public void visit(OrderedListNode node) {
        startListBox(MarkdownToNodeGenerator.STYLE_CLASS_ORDERED_LIST);
        listCount.push(1);
        visitChildren(node);
        listCount.pop();
        stopListBox();
    }

    @Override
    public void visit(ListItemNode node) {
        String bullet = "\u2022";
        if (listCount.peek() != null) {
            int i = listCount.pop();
            bullet = Integer.toString(i) + ". ";
            listCount.push(++i);
        }
        startListRow(bullet);
        visitChildren(node);
        stopListRow();
    }


    @Override
    public void visit(DefinitionListNode node) {
        //TODO System.out.println("DefinitionList Node");
        visitChildren(node);
    }

    @Override
    public void visit(DefinitionNode node) {
        //TODO System.out.println("Definition Node");
        visitChildren(node);
    }

    @Override
    public void visit(DefinitionTermNode node) {
        //TODO System.out.println("DefinitionTerm Node");
        visitChildren(node);
    }

    @Override
    public void visit(ExpLinkNode node) {
        //TODO MAKE IT A REAL HYPERLINK
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
        visitChildren(node);
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
    }

    @Override
    public void visit(RefLinkNode node) {
        //TODO MAKE IT A REAL HYPERLINK
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
        visitChildren(node);
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
    }


    @Override
    public void visit(AutoLinkNode node) {
        //TODO MAKE IT A REAL HYPERLINK
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
        addTextNode(node.getText());
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
    }

    @Override
    public void visit(MailLinkNode node) {
        //TODO MAKE IT A REAL HYPERLINK
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
        addTextNode(node.getText());
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
    }

    @Override
    public void visit(WikiLinkNode node) {
        //TODO MAKE IT A REAL HYPERLINK
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
        addTextNode(node.getText());
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_HYPERLINK);
    }


    @Override
    public void visit(ReferenceNode node) {
        // will be handled by RefLinkNode and RefImageNode
    }

    @Override
    public void visit(HeaderNode node) {
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_HEADER_BASE + node.getLevel());
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_HEADER);
        TextFlow fp = new TextFlow();
        fp.getStyleClass().setAll(cssClasses);
        currentCollector.getChildren().add(fp);
        pushNode(fp);
        visitChildren(node);
        popNode();
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_HEADER_BASE + node.getLevel());
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_HEADER);
    }

    @Override
    public void visit(ExpImageNode node) {
        ImageView image = new ImageView(node.url);

        // should have only one text type child
        SuperNode sn = (SuperNode) node.getChildren().get(0);
        String title = ((TextNode)sn.getChildren().get(0)).getText();
        Platform.runLater(() -> {
            Tooltip tt = new Tooltip(title);
            Tooltip.install(image, tt);
        });

        currentCollector.getChildren().add(image);
    }

    @Override
    public void visit(RefImageNode node) {
        String refKey = ((TextNode) node.referenceKey.getChildren().get(0)).getText();
        ReferenceNode rn = references.get(refKey);

        ImageView image = new ImageView(rn.getUrl());

        // should have only one text type child
        SuperNode sn = (SuperNode) node.getChildren().get(0);
        String title = ((TextNode)sn.getChildren().get(0)).getText();
        Platform.runLater(() -> {
            Tooltip tt = new Tooltip(title);
            Tooltip.install(image, tt);
        });

        currentCollector.getChildren().add(image);

    }

    @Override
    public void visit(RootNode node) {
        for (ReferenceNode refNode : node.getReferences()) {
            TextNode tn = (TextNode) refNode.getChildren().get(0).getChildren().get(0);
            references.put(tn.getText(), refNode);
        }
        visitChildren(node);
    }

    @Override
    public void visit(Node node) {
//        System.out.println("Node");
    }

    @Override
    public void visit(AbbreviationNode node) {
        //TODO abbreviations not supported right now
    }

    @Override
    public void visit(TableBodyNode node) {
        visitChildren(node);
    }

    @Override
    public void visit(TableCaptionNode node) {
        FlowPane fp = new FlowPane();


        Integer row = tableRowCount.pop();
        if (row > 0) {
            row++;
        }
        GridPane.setRowIndex(fp, row);
        GridPane.setColumnIndex(fp, 0);
        tableRowCount.push(row+1);

        GridPane.setColumnSpan(fp, GridPane.REMAINING);
        fp.setAlignment(Pos.BASELINE_CENTER);

        fp.getStyleClass().setAll(cssClasses);
        fp.getStyleClass().add(MarkdownToNodeGenerator.STYLE_CLASS_TABLE_CAPTION);


        TextFlow tf = new TextFlow();
        tf.setMinWidth(Region.USE_PREF_SIZE);
        tf.setMinHeight(Region.USE_PREF_SIZE);
        tf.setMaxWidth(Region.USE_PREF_SIZE);
        tf.setMaxHeight(Region.USE_PREF_SIZE);

        tf.getStyleClass().setAll(cssClasses);

        fp.getChildren().setAll(tf);

        currentCollector.getChildren().add(fp);
        pushNode(tf);
        visitChildren(node);
        popNode();
    }

    @Override
    public void visit(TableCellNode node) {
        FlowPane fp = new FlowPane();

        Integer col = tableColumnCount.pop();
        GridPane.setRowIndex(fp, tableRowCount.peek());
        GridPane.setColumnIndex(fp, col);
        HPos hpos = ((GridPane)currentCollector).getColumnConstraints().get(col).getHalignment();
        if (hpos != null) {
            switch (hpos) {
                case RIGHT:
                    fp.setAlignment(Pos.BASELINE_RIGHT);
                    break;
                case LEFT:
                    fp.setAlignment(Pos.BASELINE_LEFT);
                    break;
                case CENTER:
                    fp.setAlignment(Pos.BASELINE_CENTER);
                    break;
            }
        }
        tableColumnCount.push(col+1);

        GridPane.setColumnSpan(fp, node.getColSpan());
        fp.getStyleClass().setAll(cssClasses);

        TextFlow tf = new TextFlow();
        tf.setMinWidth(Region.USE_PREF_SIZE);
        tf.setMinHeight(Region.USE_PREF_SIZE);
        tf.setMaxWidth(Region.USE_PREF_SIZE);
        tf.setMaxHeight(Region.USE_PREF_SIZE);

        tf.getStyleClass().setAll(cssClasses);

        fp.getChildren().setAll(tf);

        currentCollector.getChildren().add(fp);
        pushNode(tf);
        visitChildren(node);
        popNode();
    }

    @Override
    public void visit(TableColumnNode node) {
        System.out.println("TableColumn Node??");
        visitChildren(node);
    }

    @Override
    public void visit(TableHeaderNode node) {
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_TABLE_HEADER);
        visitChildren(node);
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_TABLE_HEADER);
    }

    @Override
    public void visit(TableNode node) {
        tableRowCount.push(0);
        GridPane gp = new GridPane();
        gp.setHgap(0);
        gp.setVgap(0);
        gp.getStyleClass().add(MarkdownToNodeGenerator.STYLE_CLASS_TABLE);
        List<ColumnConstraints> ccl = new ArrayList<>();
        for (TableColumnNode tcn : node.getColumns()) {
            ColumnConstraints cc = new ColumnConstraints();
            switch (tcn.getAlignment()) {
                case Left:
                    cc.setHalignment(HPos.LEFT);
                    break;
                case Center:
                    cc.setHalignment(HPos.CENTER);
                    break;
                case Right:
                    cc.setHalignment(HPos.RIGHT);
                    break;
            }
            ccl.add(cc);
        }
        gp.getColumnConstraints().setAll(ccl);
        currentCollector.getChildren().add(gp);
        pushNode(gp);
        visitChildren(node);
        popNode();
        tableRowCount.pop();
    }

    @Override
    public void visit(TableRowNode node) {
        boolean odd = false;
        int row = tableRowCount.pop();
        if ((row & 0x1) == 0x1) {
            odd = true;
        }
        tableRowCount.push(row + 1);
        tableColumnCount.push(0);
        cssClasses.add(MarkdownToNodeGenerator.STYLE_CLASS_TABLE_ROW);
        if (odd) {
            cssClasses.add("odd");
        }
        visitChildren(node);
        cssClasses.remove(MarkdownToNodeGenerator.STYLE_CLASS_TABLE_ROW);
        tableColumnCount.pop();
        if (odd) {
            cssClasses.remove("odd");
        }
    }
}
