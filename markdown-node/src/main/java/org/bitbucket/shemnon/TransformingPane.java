package org.bitbucket.shemnon;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.util.function.Function;

/**
 * This node will take a value, and when that value is changed run the transformer
 * on that node
 */
public class TransformingPane<T> extends VBox {

    protected Function<T, Node> transformation;

    protected ObjectProperty<T> valueProp;

    public TransformingPane() {
        valueProp = new SimpleObjectProperty<>();
        createListeners();
    }

    public TransformingPane(Function<T, Node> transform, T initialValue) {
        this();
        transformation = transform;
        valueProp.set(initialValue);
    }

    protected void createListeners() {
        valueProp.addListener(this::sourceValueChanged);
    }

    public void recalculateTransform() {
        sourceValueChanged(valueProp, valueProp.get(), valueProp.get());
    }

    public void sourceValueChanged(ObservableValue<? extends T> prop, T oldValue, T newValue) {
        getChildren().setAll(transformation.apply(newValue));
    }

    public T getValue() {
        return valueProp.get();
    }

    public void setValue(T newValue) {
        valueProp.set(newValue);
    }

    public ObjectProperty<T> valueProperty() {
        return valueProp;
    }

    protected Function<T, Node> getTransformation() {
        return transformation;
    }

    public void setTransformation(Function<T, Node> transformation) {
        this.transformation = transformation;
    }
}
