package org.bitbucket.shemnon;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

/**
 * This thread uses a service to create the nodes
 */
public class OffThreadTransformingPane<T> extends TransformingPane<T> {

    ExecutorService nodeService;

    public OffThreadTransformingPane() {
        super();
        createService();
    }

    public OffThreadTransformingPane(Function<T, Node> transform, T initialValue) {
        super(transform, initialValue);
        createService();
    }

    private void createService() {
        nodeService = Executors.newSingleThreadExecutor();
    }

    @Override
    public void sourceValueChanged(ObservableValue<? extends T> prop, T oldValue, T newValue) {
        nodeService.submit(() -> {
            try {
                final Node n = transformation.apply(newValue);
                Platform.runLater(() -> {
                    getChildren().setAll(n);
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
