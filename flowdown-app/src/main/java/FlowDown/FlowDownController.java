package FlowDown;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.stage.FileChooser;
import javafx.stage.WindowEvent;
import org.bitbucket.shemnon.mdnode.MarkdownToNodeGenerator;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: shemnon
 * Date: 2 Feb 2013
 * Time: 11:05 PM
 */
public class FlowDownController {

    FlowDownModel model;
    FlowDownView view;
    FileChooser fileChooser;

    public FlowDownController() {
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Markdown Documents", "*.md", "*.markdown")
        );
    }


    public void newDoc(ActionEvent e) {
        if (model.dirty.get()) {
            //TODO ask to save dialog
        }
        model.content.set("");
    }

    public void open(ActionEvent e) {
        File f = fileChooser.showOpenDialog(view.getWindow());
        System.out.println(f);
        new Thread(() -> {
            if (f != null) {
                // This is way simpler in groovy
                try (
                    FileReader fr = new FileReader(f);
                    StringWriter sw = new StringWriter()
                ) {
                    char[] buffer = new char[4096];
                    int size = 0;
                    while ((size = fr.read(buffer)) > 0) {
                        sw.write(buffer, 0, size);
                    }
                    sw.close();
                    fr.close();
                    Platform.runLater(() -> model.content.set(sw.toString()));
                    model.fileProp.set(f);
                } catch (IOException ignore) {}
            }
        }).start();
    }

    public void save(ActionEvent e) {
        if (model.fileProp.get() == null) {
            saveAs(e);
        } else {
            saveFile(model.fileProp.get());
        }
    }

    public void saveAs(ActionEvent e) {
        saveFile(fileChooser.showSaveDialog(view.getWindow()));

    }

    protected void saveFile(final File f) {
        if (f != null) {
            new Thread(() -> {
                try (
                    StringReader stringReader = new StringReader(model.content.get());
                    FileWriter fw = new FileWriter(f)
                ) {
                    char[] buffer = new char[4096];
                    int size = 0;
                    while ((size = stringReader.read(buffer)) > 0) {
                        fw.write(buffer, 0, size);
                    }
                    stringReader.close();
                    fw.close();
                } catch (IOException ignore) {}

            }).start();
        }
    }

    public void quit(ActionEvent e) {
        exit();
    }

    public void closeWindow(WindowEvent e) {
        exit();
    }

    public void refreshCSS(ActionEvent e) {
        if (view != null) {
            String newStylesheet = model.cssFile.get();
            String result = resolveStylesheet(newStylesheet);
            view.setMarkDownStylesheets(result);
        }
    }

    public void updateConfig(ActionEvent e) {
        view.setParserFlags(model.getParserFlags());
    }

    private String resolveStylesheet(String newStylesheet) {
        String result = null;

        try {
            // first try as a URL
            result = new URL(newStylesheet).toExternalForm();
        } catch (MalformedURLException ignore) {
        }

        if (result == null) {
            // next try as a file
            File f = new File(newStylesheet);
            if (f.exists() && f.isFile()) {
                result = f.toURI().toString();
            }
        }

        if (result == null) {
            // next resolve against our classpath
            try {
                result = FlowDownView.class.getResource(newStylesheet).toExternalForm();
            } catch (NullPointerException ignore) {
            }
        }

        if (result == null) {
            // finally resolve against the library classpath
            try {
                result = MarkdownToNodeGenerator.class.getResource(newStylesheet).toExternalForm();
            } catch (NullPointerException ignore) {
            }
        }
        return result;
    }


    public void showSourceOnly(ActionEvent e) {
         model.splitpanePosition.set(1.0);
    }

    public void showSourceAndResult(ActionEvent e) {
        model.splitpanePosition.set(0.5);
    }

    public void showResultOnly(ActionEvent e) {
        model.splitpanePosition.set(0.0);
    }

    /**
     * No dirty checking, goes down hard
     */
    protected void exit() {
        Platform.exit();
        System.out.println("DIE DIE DIE");
        System.exit(0);
    }
}
