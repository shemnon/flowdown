package FlowDown;

import javafx.beans.property.*;
import org.pegdown.Extensions;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: shemnon
 * Date: 2 Feb 2013
 * Time: 10:48 PM
 */
public class FlowDownModel {

    ObjectProperty<File> fileProp = new SimpleObjectProperty<>();

    StringProperty content = new SimpleStringProperty("");

    StringProperty cssFile = new SimpleStringProperty("");

    BooleanProperty dirty = new SimpleBooleanProperty(false);

    ListProperty<String> cssURLs = new SimpleListProperty<>();

    DoubleProperty splitpanePosition = new SimpleDoubleProperty(0.5);

    // pegdown options

    BooleanProperty optionHardLineWraps = new SimpleBooleanProperty(false);
    BooleanProperty optionAutoLinks = new SimpleBooleanProperty(false);
    BooleanProperty optionWikiLinks = new SimpleBooleanProperty(false);
    BooleanProperty optionSmartDashesAndDots = new SimpleBooleanProperty(false);
    BooleanProperty optionSmartQuotes = new SimpleBooleanProperty(false);
    BooleanProperty optionAbbreviations = new SimpleBooleanProperty(false);
    BooleanProperty optionDefinitions = new SimpleBooleanProperty(false);
    BooleanProperty optionFencedCodeBlocks = new SimpleBooleanProperty(false);
    BooleanProperty optionTables = new SimpleBooleanProperty(false);

    public int getParserFlags() {
        int flags = 0;
        if (optionAbbreviations.get()) flags |= Extensions.ABBREVIATIONS;
        if (optionAutoLinks.get()) flags |= Extensions.AUTOLINKS;
        if (optionDefinitions.get()) flags |= Extensions.DEFINITIONS;
        if (optionFencedCodeBlocks.get()) flags |= Extensions.DEFINITIONS;
        if (optionHardLineWraps.get()) flags |= Extensions.HARDWRAPS;
        if (optionSmartQuotes.get()) flags |= Extensions.QUOTES;
        if (optionSmartDashesAndDots.get()) flags |= Extensions.SMARTS;
        if (optionTables.get()) flags |= Extensions.TABLES;
        if (optionWikiLinks.get()) flags |= Extensions.WIKILINKS;

        return flags;
    }

}
