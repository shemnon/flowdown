/*
 * Copyright (c) 2012, Danno Ferrin
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Danno Ferrin nor the
 *         names of contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package FlowDown;

import javafx.application.Application;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FlowDownModel model = new FlowDownModel();
        FlowDownController controller = new FlowDownController();

        controller.model = model;

        FXMLLoader loader = new FXMLLoader(Main.class.getResource("FlowDownView.fxml"));
        ObservableMap<String, Object> namespace = loader.getNamespace();

        namespace.put("onNewDoc", (EventHandler<ActionEvent>) controller::newDoc);
        namespace.put("onOpen", (EventHandler<ActionEvent>) controller::open);
        namespace.put("onSave", (EventHandler<ActionEvent>) controller::save);
        namespace.put("onSaveAs", (EventHandler<ActionEvent>) controller::saveAs);
        namespace.put("onQuit", (EventHandler<ActionEvent>) controller::quit);
        namespace.put("onRefreshCSS", (EventHandler<ActionEvent>) controller::refreshCSS);
        namespace.put("onShowSourceOnly", (EventHandler<ActionEvent>) controller::showSourceOnly);
        namespace.put("onShowSourceAndResult", (EventHandler<ActionEvent>) controller::showSourceAndResult);
        namespace.put("onShowResultOnly", (EventHandler<ActionEvent>) controller::showResultOnly);
        namespace.put("onUpdateConfig", (EventHandler<ActionEvent>) controller::updateConfig);


        loader.load();
        FlowDownView view = loader.getController();
        controller.view = view;

        model.content.bindBidirectional(view.sourceTextArea.textProperty());
        model.cssURLs.addListener((ListChangeListener<String>) change -> {
            if (view.contentScrollPane.getContent() instanceof Parent) {
                // Sometimes it is fired before content is set, so guard against it
                ((Parent)view.contentScrollPane.getContent()).getStylesheets().setAll(change.getList());
            }
        });
        model.cssFile.bind(view.styleCombo.getSelectionModel().selectedItemProperty());


        model.splitpanePosition.addListener((observableValue, oldNumber, newNumber) ->
                view.contentSplitPane.setDividerPosition(0, newNumber.doubleValue()));

        Parent p = loader.getRoot();

        model.optionAutoLinks.bindBidirectional(view.micbOptionAutoLinks.selectedProperty());
        model.optionHardLineWraps.bindBidirectional(view.micbOptionHardLineWraps.selectedProperty());
        model.optionSmartDashesAndDots.bindBidirectional(view.micbOptionSmartDashesAndDots.selectedProperty());
        model.optionSmartQuotes.bindBidirectional(view.micbOptionSmartQuotes.selectedProperty());
        model.optionTables.bindBidirectional(view.micbOptionTables.selectedProperty());
        model.optionWikiLinks.bindBidirectional(view.micbOptionWikiLinks.selectedProperty());


        Scene scene = new Scene(p);

        stage.setTitle("FlowDown"); //TODO bind title
        stage.setScene(scene);

        stage.setOnCloseRequest(controller::closeWindow);

        stage.sizeToScene();
        stage.show();
    }
   
    
    public static void main(String[] args) {
        Application.launch(args);
    }
    
}
