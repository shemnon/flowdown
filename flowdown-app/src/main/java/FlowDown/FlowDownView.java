/*
 * Copyright (c) 2012, Danno Ferrin
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Danno Ferrin nor the
 *         names of contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package FlowDown;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.stage.Window;
import org.bitbucket.shemnon.mdnode.MarkdownNode;
import org.bitbucket.shemnon.mdnode.MarkdownToNodeGenerator;

import java.net.URL;
import java.util.ResourceBundle;


public class FlowDownView
    implements Initializable {

    MarkdownNode mdNode;

    @FXML
    ScrollPane contentScrollPane;

    @FXML
    SplitPane contentSplitPane;

    @FXML
    CheckMenuItem micbOptionAutoLinks;

    @FXML
    CheckMenuItem micbOptionHardLineWraps;

    @FXML
    CheckMenuItem micbOptionSmartDashesAndDots;

    @FXML
    CheckMenuItem micbOptionSmartQuotes;

    @FXML
    CheckMenuItem micbOptionTables;

    @FXML
    CheckMenuItem micbOptionWikiLinks;

    @FXML
    private Button refreshButton;

    @FXML
   TextArea sourceTextArea;

    @FXML
    ComboBox<String> styleCombo;

    @FXML
    private Pane toolbarSpacer;


    public void setParserFlags(int flags) {
        mdNode.setParserFlags(flags);
    }

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {

        sourceTextArea.textProperty().addListener((observableValue, oldString, newString) -> updateMarkdown(newString));

        styleCombo.getItems().setAll(MarkdownNode.STYLESHEET_FLOWDOWN, MarkdownNode.STYLESHEET_GITHUB, "Test.css");
        styleCombo.setValue(MarkdownNode.STYLESHEET_DEFAULT);

        mdNode = new MarkdownNode();
        mdNode.setValue(sourceTextArea.getText());
        contentScrollPane.setContent(mdNode);

        HBox.setHgrow(toolbarSpacer, Priority.ALWAYS);
    }


    private void updateMarkdown(String newString) {
        mdNode.setValue(newString);
    }

    public Window getWindow() {
        return contentScrollPane.getScene().getWindow();
    }


    public void setMarkDownStylesheets(String... stylesheets) {
       mdNode.getStylesheets().clear();
       mdNode.getStylesheets().setAll(stylesheets);
    }
}

