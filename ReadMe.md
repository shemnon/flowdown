This is a Toy app demonstrating the new [TextFlow](https://wikis.oracle.com/display/OpenJDK/Rich+Text) node in JavaFX.

To run this app...

    ./gradlew run

The Markdown is parsed via the [PegDown](http://pegdown.org/) parser, and packaged into TextFlows via the visitor.
PegDown options that are implemented are turned on in the configuration popup.